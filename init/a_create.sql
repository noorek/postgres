CREATE TYPE status_type AS ENUM ('pending', 'completed', 'rejected');

CREATE TABLE carts(
  id VARCHAR unique,
  cart_product_ids VARCHAR(50)[],
  total_price real,
  status status_type
);

CREATE TABLE products(
  id SERIAL PRIMARY KEY,
  name VARCHAR(50),
  image_url VARCHAR,
  price real,
  amount INT
);

CREATE TABLE cart_products(
  id VARCHAR(50),
  product_id INT REFERENCES products(id),
  amount INT
);